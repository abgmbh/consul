variable "prefix" {}
variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_region" {}
variable "subnet_id" {}
variable "key_name" {}
variable "vpc_name" {}

variable "linux_server_ami" { default = "ami-01efb339f953fdf36" }
variable "linux_server_instance_type" { default = "t2.micro" }
variable "linux_server_pkgs" { default = ["httpd", "net-tools", "unzip", "git"] }
variable "webserver_count" { default = 2 }

variable "default_ec2_tags" {
  description = "Default set of tags to apply to EC2 instances"
  type        = map(any)
  default = {
    Owner       = "cz"
    Environment = "Demo"
    SupportTeam = "ANZSE"
    Contact     = "cz@ab.gmbh"
  }
}
