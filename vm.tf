resource "aws_instance" "consul-server" {
  ami                         = var.linux_server_ami
  instance_type               = var.linux_server_instance_type
  subnet_id                   = var.subnet_id
  associate_public_ip_address = true
  key_name                    = var.key_name
  user_data = templatefile("consul-server.sh.tpl",
    {
      linux_server_pkgs = var.linux_server_pkgs
    }
  )

  tags = {
    for k, v in merge({
      Name = format("%s-consul-server", var.prefix)
      },
    var.default_ec2_tags) : k => v
  }
}

resource "aws_instance" "web-server" {
  ami                         = var.linux_server_ami
  instance_type               = var.linux_server_instance_type
  subnet_id                   = var.subnet_id
  associate_public_ip_address = true
  key_name                    = var.key_name
  user_data = templatefile("web-server.sh.tpl",
    {
      linux_server_pkgs = var.linux_server_pkgs
      consul_address    = aws_instance.consul-server.private_ip
    }
  )
  count = var.webserver_count

  tags = {
    for k, v in merge({
      Name = format("%s-web-server-${count.index}", var.prefix)
      },
    var.default_ec2_tags) : k => v
  }
}
