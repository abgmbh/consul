output "consul_server_ip" {
  value = aws_instance.consul-server.public_ip
}
output "web_server_ip" {
  value = aws_instance.web-server.*.public_ip
}
